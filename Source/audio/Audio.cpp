/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::setKick(float Hz, float attack, float release, float amp)
{
    kick.setDrumSynth(Hz, amp, 1);
    kick.setEG(attack, release);
    
}

void Audio::setSnare(float Hz, float release, float amp)
{
    snare.setDrumSynth(Hz, amp, 0);
    snare.setEG(0, release);
    snareNoise.setEG(0, release);
}

void Audio::setHiTom(float Hz,float release, float amp)
{
    hiTom.setDrumSynth(Hz, amp, 0);
    hiTom.setEG(0, release);
}

void Audio::setLowTom(float Hz, float release, float amp)
{
    lowTom.setDrumSynth(Hz, amp, 0);
    lowTom.setEG(0, release);
}

void Audio::setClHiHat(float release, float amp)
{
    lowTom.setDrumSynth(0, amp, 0);
    lowTom.setEG(0, release);
}

void Audio::setOPHiHat(float release, float amp)
{
    lowTom.setDrumSynth(0, amp, 0);
    lowTom.setEG(0, release);
}

//void Audio::setSample1(const String& absolutePath)
//{
//
//}
//
//void Audio::setSample2(const String& absolutePath)
//{
//    
//}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    if (message.isNoteOn())
    {
        if (message.getNoteNumber() == 12)
        {
            kick.trigger();
        }
        
        if (message.getNoteNumber() == 14)
        {
            snare.trigger();
        }
    }
    
    if (message.isNoteOff())
    {
        
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        *outL = kick.GetDrumSample();
                //+ snare.GetDrumSample() + hiTom.GetDrumSample() + lowTom.GetDrumSample() +
                //opHiHat.GetDrumSample() + clHiHat.GetDrumSample() + clap.processSample() + cowBell.processSample();
        *outR = *outL;

        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}



void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    
}

void Audio::audioDeviceStopped()
{

}