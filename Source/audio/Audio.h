/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../AudioNode/OSC/SineOSC/SineOSC.hpp"
#include "../AudioNode/EG/EG.hpp"
#include "../AudioNode/OSC/SineOSC/SineOSC.hpp"
#include "../Sampler/Sampler.hpp"
#include "../Drum/SineDrumVoice.hpp"
#include "../Drum/SquareDrumVoice.hpp"
#include "../Drum/TriangleDrumVoice.hpp"
#include "../Drum/NoiseDrumVoice.hpp"


class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager;}
    
    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    
    void setKick(float Hz, float attack, float release, float amp);
    void setSnare(float Hz, float release, float amp);
    void setHiTom(float Hz, float release, float amp);
    void setLowTom(float Hz, float release, float amp);
    void setClHiHat(float release, float amp);
    void setOPHiHat(float release, float amp);
    
//    void setSample1(const String& absolutePath);
//    void setSample2(const String& absolutePath);

    
    
    void audioDeviceStopped() override;
protected:
    AudioDeviceManager  audioDeviceManager;
    
    Sampler             clap,
                        cowBell;
    
    SineDrumVoice       kick,
                        hiTom,
                        lowTom,
                        snare;
    
    NoiseDrumVoice      snareNoise,
                        clHiHat,
                        opHiHat;
};



#endif  // AUDIO_H_INCLUDED
