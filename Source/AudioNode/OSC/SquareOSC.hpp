//
//  Square.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef SquareOSC_hpp
#define SquareOSC_hpp

#include <stdio.h>
#include "../OSC/OSC.hpp"
#include <math.h>

class SquareOSC : public OSC
{
public:
    virtual ~SquareOSC();
    virtual float renderWaveShape (const float currentPhase);
    
private:
    
};

#endif /* SquareOSC_hpp */
