//
//  TriangleOSC.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#include "TriangleOSC.hpp"
TriangleOSC:: ~TriangleOSC()
{
    
}
/**Sinewave generated in Render WaveShape. Current phase is passed from getSample in the OSC base class*/
float TriangleOSC::renderWaveShape (const float currentPhase)
{
    /** The current code Is currently incorrect and does not produce a triangle wave*/
    if (currentPhase < M_1_PI/ 2)
    {
        return  1 * currentPhase;

    }
    else if (currentPhase > M_1_PI /1 && currentPhase < M_1_PI * 0.75)
    {
        return -1 *currentPhase + 2;
    }
    else
    {
        return 1 * currentPhase - 2;
    }
}