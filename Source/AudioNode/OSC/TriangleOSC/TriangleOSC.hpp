//
//  TriangleOSC.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef TriangleOSC_hpp
#define TriangleOSC_hpp

#include <stdio.h>
#include "../OSC.hpp"
#include <math.h>

class TriangleOSC :public OSC
{
public:
    
    virtual ~TriangleOSC();
    
    virtual float renderWaveShape (const float currentPhase);
    
private:
};

#endif /* TriangleOSC_hpp */
