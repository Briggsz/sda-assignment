//
//  OSC.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 22/01/2016.
//
//

#ifndef OSC_hpp
#define OSC_hpp

#include <stdio.h>
#include <math.h>
#include "../AudioNode.hpp"

/**OSC is the base class for all oscillators. The Render Wave shape is pure virtual and it is in this function that the wave will generated */
class OSC : public AudioNode
{
public:
    
                    OSC();
    virtual         ~OSC();
    void            reset();
    void            setFrequency(float setFrequency);
    virtual float   renderWaveShape (const float currentPhase) = 0;
    
    float   getSample() override;
    
private:
    const float twoPI = M_1_PI*2;
    
    float   phasePosition,
            phaseIncrement;
};

#endif /* OSC_hpp */
