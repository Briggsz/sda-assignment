//
//  Square.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#include "SquareOSC.hpp"

SquareOSC::~SquareOSC()
{
    
}

/**SquareWave generated in Render WaveShape.*/
float SquareOSC::renderWaveShape (const float currentPhase)
{
    if (currentPhase < M_1_PI)
    {
        return 1;
    }
    
    else
    {
        return -1;
    }
    
}