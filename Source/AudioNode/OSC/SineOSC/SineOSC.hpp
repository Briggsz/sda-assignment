//
//  SineOSC.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 30/12/2015.
//
//

#ifndef SineOSC_hpp
#define SineOSC_hpp
#include "../../OSC/OSC.hpp"


#include <stdio.h>
/**Sine Class Inherits from OSC */
class SineOSC:public OSC
{
public:
    
    virtual ~SineOSC();
    
    virtual float renderWaveShape (const float currentPhase);
    
private:
};


#endif /* SineOSC_hpp */