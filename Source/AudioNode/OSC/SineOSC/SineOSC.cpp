//
//  SineOSC.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 30/12/2015.
//
//

#include "SineOSC.hpp"

SineOSC:: ~SineOSC()
{
    
}

/**Sinewave generated in Render WaveShape. Current phase is passed from getSample in the OSC base class*/
float SineOSC::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}