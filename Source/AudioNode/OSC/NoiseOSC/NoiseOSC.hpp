//
//  NoiseOSC.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef NoiseOSC_hpp
#define NoiseOSC_hpp

#include <stdio.h>
#include <math.h>
#include "../OSC.hpp"

class NoiseOSC : public OSC
{
public:
    virtual ~NoiseOSC();
    virtual float renderWaveShape (const float currentPhase);
    
private:
    
};

#endif /* NoiseOSC_hpp */
