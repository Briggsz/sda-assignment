//
//  OSC.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 22/01/2016.
//
//

#include "OSC.hpp"

OSC::OSC()
{
    reset();
}

OSC::~OSC()
{
    
}


void OSC::setFrequency(float setFrequency)
{
    /**equation works out the phase increment*/
    phaseIncrement = ((2 * M_1_PI) * setFrequency)/sampleRate;
}

/**Resets OSC to Defults */
void OSC::reset()
{
    phasePosition = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
    setAmplitude (1.f);
}

/**The get Sample returns audio thee audio signals generated in the render waveshape function*/
float OSC::getSample()
{
    /**renderWaveshape is called here and the phase position is passed to it*/
    
    float outPut = renderWaveShape (phasePosition) * 0.5 ;
    
    /**phase Position is then incremented*/
    phasePosition = phasePosition + phaseIncrement ;
    
    /** This if statement loops the current phase so that the phase position is never higher than 2 Pi*/
    if(phasePosition  > (2.f * M_PI))
        phasePosition -= (2.f * M_PI);
    
    return outPut * amplitude;
}