//
//  AudioNode.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 26/01/2016.
//
//

#include "AudioNode.hpp"
AudioNode::AudioNode()
{
    /** Amplitude to 1*/
    amplitude = 1.f;
    
    /** Sets the sample rate at the defult 44100*/
    sampleRate = 44100;
}

AudioNode::~AudioNode()
{
    
}

/**Functions to change sample Rate*/
void AudioNode::setSampleRate(float sr)
{
    sampleRate = sr;
}

/**Sets Amplitude */
void AudioNode::setAmplitude(float a)
{
    amplitude = a;
}

