//
// EG.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 25/01/2016.
//
//

#include "EG.hpp"
EG::EG()
{
    attackTime = 0.f;
    releaseTime = 0.f;
    counter = 0;
}

EG::~EG()
{
    
}

void EG::trigger()
{
    counter = 0;
}

void EG::setAttack (float timeMs)
{
    attackTime = timeMs;
}
void EG::setRelease(float timeMs)
{
    releaseTime = timeMs;
    
}
/**The Get sample function is overriden in the EG class */
float EG::getSample()
{
    if (counter < sampleRate *2)
    {
        counter++;
    }
    
    /**This is the Attack portion of the EG*/
    float time = (float)counter / sampleRate;
    
    float output = 0.f;
    
    if (time < attackTime)
    {
        return (attackTime * 1000) * time;
    }
    
    
    /**The Release portion of the EG. currently the release portion can not be controlled via the ui*/

    else if (time < 1.f)
    {
        return 2 - 2 * time;
    }
    return output;
}