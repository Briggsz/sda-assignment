//
//  OSCEG.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 25/01/2016.
//
//

#ifndef EG_hpp
#define EG_hpp
#include "../../AudioNode/AudioNode.hpp"
#include <math.h>
#include <stdio.h>
/**As the EG similar to a oscillator it shares 3 of the same features. amplitude, sample rate and getSample()*/
class EG : public AudioNode
{
public:
    EG();
    virtual         ~EG();
    void            setAttack (float timeMs);
    void            setRelease(float timeMs);
    void            trigger();
    
    virtual float   getSample() override;

private:
    float   attackTime,
            releaseTime;
    
    int counter;
    
};
#endif /* EG_hpp */


