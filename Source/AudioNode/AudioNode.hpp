//
//  AudioNode.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 26/01/2016.
//
//

/** Base Class for any Class that Processes Audio **/

#ifndef AudioNode_hpp
#define AudioNode_hpp

#include <stdio.h>
#include <math.h>

class AudioNode
{
public:
                    AudioNode();
    virtual         ~AudioNode();
    void            setSampleRate(float samplpeRate);
    void            setAmplitude(float Amp);
    /**The get sample function is were any audio is returned*/
    virtual float   getSample() = 0;
    
    
    
protected:
    int             sampleRate;
    float           amplitude;
};
#endif /* AudioNode_hpp */
