//
//  Sampler.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 25/01/2016.
//
//

#ifndef Sampler_hpp
#define Sampler_hpp

#include "../JuceLibraryCode/JuceHeader.h"
#include "../AudioNode/AudioNode.hpp"
#include <stdio.h>

class Sampler: public  AudioNode,
                       AudioSampleBuffer
{
public:
        Sampler();
        ~Sampler();
    void setPlayState (void);
    virtual float getSample (void);

    void fileload();
    
    
    
private:
    AudioSampleBuffer audioSampleBuffer;
    AudioFormatManager formatManager;
    
    Atomic<int> recordState;
    Atomic<int> playState;
    
    int bufferSize; //constant
    
    unsigned int bufferPosition;
};

#endif /* Sampler_hpp */
