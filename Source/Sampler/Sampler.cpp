//
//  Sampler.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 25/01/2016.


#include "Sampler.hpp"

Sampler::Sampler()
{
    /**intialises the play state to false*/
    playState = false;
    
    /**Sets Buffer to 0 position */
    bufferPosition = 0;
    
    audioSampleBuffer.clear();
    
    /**Adds a format to avaible file formats*/
    formatManager.registerBasicFormats();
    
    /**Creates a file from a specific file location*/
    File file ("/Users/Rob/Documents/UNI/Software Development/Assignment.1/Source/Sampler/Clap-707.aif");
    
    /**Creates a read for the file. because it is a Scoped it is deleted after it is used*/
    ScopedPointer<AudioFormatReader> reader = formatManager.createReaderFor(file);
    
    /**Sets the Size of the buffer and number of channels via the reader*/
    audioSampleBuffer.setSize (reader->numChannels, reader->lengthInSamples);
    reader->read (&audioSampleBuffer, 0, reader->lengthInSamples, 0, true, false);

}
Sampler::~Sampler()
{
    
}

/**Set play state sets the play state to true*/
void Sampler::setPlayState ()
{
        playState = true;
        DBG("playstate True" );
}


float Sampler::getSample()
{
    /**reset the output*/
    float output = 0.f;
    
    if (playState.get() == true)
    {
        /**if playstate is true theoutput is assigned to the audio buffer reader */
        output = *audioSampleBuffer.getReadPointer(0, bufferPosition);
        
        //*increment and cycle the buffer counter*/
        ++bufferPosition;
        
        /**If buffer position is the same size as buffer size, then the sample has come to the end and the play state is set to 0 and buffer position is reset*/
        if (bufferPosition == bufferSize)
            playState = false;
        bufferPosition = 0;
        
    }
    return output * amplitude;
}


void Sampler::fileload()
{
    
}


#include "Sampler.hpp"
