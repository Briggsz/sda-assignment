//
//  SquareDrumVoice.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#include "SquareDrumVoice.hpp"
SquareDrumVoice::SquareDrumVoice(void)
{
    
}

SquareDrumVoice::~SquareDrumVoice(void)
{
    
}

/**The Drum Voice synth is set here. There is a Sub function  which will activate != 0 is passed to it*/
void SquareDrumVoice::setDrumSynth(float Hz, float amp, int sub)
{
    square1.setFrequency(Hz);
    square1.setAmplitude(amp);
    subSquare.setFrequency(Hz);
    subSquare.setAmplitude(amp);
}

float SquareDrumVoice::GetDrumSample(void)
{
    /**If statement checks if sub is on or of. the getSample function of both the OSC and EG are returned and multiplied together*/
    if (subSwitch == 0)
    {
        return square1.getSample() * EG1.getSample();
    }
    else
        return square1.getSample() + subSquare.getSample() * EG1.getSample();
    
}