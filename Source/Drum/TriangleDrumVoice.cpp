//
//  TriangleDrumVoice.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#include "TriangleDrumVoice.hpp"
TriangleDrumVoice::TriangleDrumVoice(void)
{
    
}

TriangleDrumVoice::~TriangleDrumVoice(void)
{
    
}

/**The Drum Voice synth is set here. There is a Sub function  which will activate != 0 is passed to it*/
void TriangleDrumVoice::setDrumSynth(float Hz, float amp, int sub)
{
    triangle1.setFrequency(Hz);
    triangle1.setAmplitude(amp);
    subTriangle.setFrequency(Hz);
    subTriangle.setAmplitude(amp);
}

float TriangleDrumVoice::GetDrumSample(void)
{
    /**If statement checks if sub is on or of. the getSample function of both the OSC and EG are returned and multiplied together*/
    if (subSwitch == 0)
    {
        return triangle1.getSample() * EG1.getSample();
    }
    else
        return triangle1.getSample() + subTriangle.getSample() * EG1.getSample();
   
}