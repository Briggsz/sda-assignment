//
//  SineDrumVoice.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 26/01/2016.
//
//

#ifndef SineDrumVoice_hpp
#define SineDrumVoice_hpp

#include <stdio.h>
#include "../Drum/DrumVoice.hpp"
#include "../AudioNode/OSC/SineOSC/SineOSC.hpp"


/**Each drum those has a individule OSC which is set in the setDrumSynth 
 Function, and audio is returned through the geteDrumSample function*/
class SineDrumVoice: public DrumVoice
{
public:
    
                    SineDrumVoice(void);
    virtual         ~SineDrumVoice(void);
    virtual void    setDrumSynth(float Hz, float amp, int sub);
    virtual float   GetDrumSample(void);
    
private:
    SineOSC sine1,
            subSine;
    
    int     subSwitch;
};

#endif /* SineDrumVoice_hpp */
