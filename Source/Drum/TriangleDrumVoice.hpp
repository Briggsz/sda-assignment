//
//  TriangleDrumVoice.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef TriangleDrumVoice_hpp
#define TriangleDrumVoice_hpp

#include <stdio.h>
#include "../Drum/DrumVoice.hpp"
#include "../AudioNode/OSC/TriangleOSC/TriangleOSC.hpp"

/**Each drum those has a individule OSC which is set in the setDrumSynth
 Function, and audio is returned through the geteDrumSample function*/
class TriangleDrumVoice: public DrumVoice
{
public:
    
                    TriangleDrumVoice(void);
    virtual         ~TriangleDrumVoice(void);
    virtual void    setDrumSynth(float Hz, float amp, int sub);
    virtual float   GetDrumSample(void);
    
private:
    TriangleOSC triangle1,
                subTriangle;
    
    int         subSwitch;
    
};
#endif /* TriangleDrumVoice_hpp */
