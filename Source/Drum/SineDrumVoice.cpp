//
//  SineDrumVoice.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 26/01/2016.
//
//

#include "SineDrumVoice.hpp"
SineDrumVoice::SineDrumVoice(void)
{
    
}

SineDrumVoice::~SineDrumVoice(void)
{
    
}

/**The Drum Voice synth is set here. There is a Sub function  which will activate != 0 is passed to it*/
void SineDrumVoice::setDrumSynth(float Hz, float amp, int sub)
{
    sine1.setFrequency(Hz);
    sine1.setAmplitude(amp);
    subSine.setFrequency(Hz/2);
    subSine.setAmplitude(amp);
    subSwitch = sub;
    
}

float SineDrumVoice::GetDrumSample(void)
{
    /**If statement checks if sub is on or of. the getSample function of both the OSC and EG are returned and multiplied together*/
    if (subSwitch == 0)
    {
        return sine1.getSample() * EG1.getSample();
    }
    else
        return sine1.getSample() + subSine.getSample() * EG1.getSample();
}