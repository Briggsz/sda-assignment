//
//  SquareDrumVoice.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef SquareDrumVoice_hpp
#define SquareDrumVoice_hpp

#include <stdio.h>
#include "../Drum/DrumVoice.hpp"
#include "../AudioNode/OSC/SquareOSC.hpp"

/**Each drum those has a individule OSC which is set in the setDrumSynth
 Function, and audio is returned through the geteDrumSample function*/
class SquareDrumVoice: public DrumVoice
{
public:
    
                    SquareDrumVoice(void);
    virtual         ~SquareDrumVoice(void);
    virtual void    setDrumSynth(float Hz, float amp, int sub) = 0;
    virtual float   GetDrumSample(void);
    
private:
    SquareOSC   square1,
                subSquare;
    
    int         subSwitch;
    
};
#endif /* SquareDrumVoice_hpp */
