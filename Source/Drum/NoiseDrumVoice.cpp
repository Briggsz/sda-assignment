//
//  NoiseDrumVoice.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#include "NoiseDrumVoice.hpp"

/**Each drum those has a individule OSC which is set in the setDrumSynth
 Function, and audio is returned through the geteDrumSample function*/
NoiseDrumVoice::NoiseDrumVoice(void)
{
    
}

NoiseDrumVoice::~NoiseDrumVoice(void)
{
    
}

void NoiseDrumVoice::setDrumSynth(float Hz, float amp, int sub)
{
    
}

float NoiseDrumVoice::GetDrumSample(void)
{
    /**the getSample function of both the OSC and EG are returned and multiplied together*/
    return noise.getSample() * EG1.getSample();

}