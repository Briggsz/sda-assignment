//
//  NoiseDrumVoice.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 27/01/2016.
//
//

#ifndef NoiseDrumVoice_hpp
#define NoiseDrumVoice_hpp

#include <stdio.h>
#include "../Drum/DrumVoice.hpp"
#include "../AudioNode/OSC/NoiseOSC/NoiseOSC.hpp"

class NoiseDrumVoice: public DrumVoice
{
public:
    
                    NoiseDrumVoice(void);
    virtual         ~NoiseDrumVoice(void);
    virtual void    setDrumSynth(float Hz, float amp, int sub);
    virtual float   GetDrumSample(void);
    
private:
    NoiseOSC noise;
    
};

#endif /* NoiseDrumVoice_hpp */
