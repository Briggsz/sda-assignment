//
//  Drum.hpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 22/01/2016.
//
//

#ifndef DrumVoice_hpp
#define DrumVoice_hpp

#include <stdio.h>
#include "../AudioNode/EG/EG.hpp"
/**The Drum Voice Base class Sets a template for individual drum voices. The base class composes of a EG */
class DrumVoice:public EG
{
public:
                    DrumVoice();
    virtual         ~DrumVoice();
    void            trigger();
    void            setEG(float attack, float release);
    /**The set Drum Synth is Pure virtual because in each individual voice will require a different OSC*/
    virtual void    setDrumSynth(float Hz, float amp, int sub) = 0;
    /**Because each drum voice uses a different OSC each Voice needs a tailored GetdrumSample function*/
    virtual float   GetDrumSample(void) = 0;
    
protected:
    EG      EG1;
    
    
};

#endif /* DrumVoice_hpp */
