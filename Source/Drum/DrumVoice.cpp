//
//  Drum.cpp
//  JuceBasicAudio
//
//  Created by Robert Briggs on 22/01/2016.
//
//

#include "DrumVoice.hpp"

DrumVoice::DrumVoice(void)
{
    
}

DrumVoice::~DrumVoice(void)
{
    
}

/**The trigger Funnction will trigger the EG which process will be apllied in the getDrumSample*/
void DrumVoice::trigger()
{
    EG1.trigger();
}
/**Sets the Drum Voices EG*/
void DrumVoice::setEG(float attack, float release)
{
    EG1.setAttack(attack);
    EG1.setRelease(release);
}

