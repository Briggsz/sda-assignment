/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_) : audio (audio_)
{
    setSize (1000, 500);
    
    addAndMakeVisible(masterVolume);
    masterVolume.setSliderStyle(Slider::Rotary);
    masterVolume.addListener(this);
    
    //Kick
    addAndMakeVisible(kickPitch);
    kickPitch.setSliderStyle(Slider::Rotary);
    kickPitch.addListener(this);
    
    addAndMakeVisible(kickAttack);
    kickAttack.setSliderStyle(Slider::Rotary);
    kickAttack.addListener(this);
    
    addAndMakeVisible(kickRelease);
    kickRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(kickAmp);
    kickAmp.setSliderStyle(Slider::LinearBarVertical);
    
    //Snare
    addAndMakeVisible(snarePitch);
    snarePitch.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(snareNoise);
    snareNoise.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(snareRelease);
    snareRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(snareNoise);
    snareNoise.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(snareAmp);
    snareAmp.setSliderStyle(Slider::LinearBarVertical);
    
    //Hi Tom
    addAndMakeVisible(hiPitch);
    hiPitch.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(hiRelease);
    hiRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(hiAmp);
    hiAmp.setSliderStyle(Slider::LinearBarVertical);
    
    //low Tom
    addAndMakeVisible(lowPitch);
    lowPitch.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(lowRelease);
    lowRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(lowAmp);
    lowAmp.setSliderStyle(Slider::LinearBarVertical);
    
    //HiHats
    addAndMakeVisible(chihatRelease);
    chihatRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(ohihatRelease);
    ohihatRelease.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(hihatTone);
    hihatTone.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(chihatAmp);
    chihatAmp.setSliderStyle(Slider::LinearBarVertical);
    
    addAndMakeVisible(ohihatAmp);
    ohihatAmp.setSliderStyle(Slider::LinearBarVertical);
    
    //Clap/Cowbell
    addAndMakeVisible(clapPitch);
    clapPitch.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(clapAmp);
    clapAmp.setSliderStyle(Slider::LinearBarVertical);
    
    addAndMakeVisible(cowBellPitch);
    cowBellPitch.setSliderStyle(Slider::Rotary);
    
    addAndMakeVisible(cowBellAmp);
    cowBellAmp.setSliderStyle(Slider::LinearBarVertical);
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    kickPitch.setBounds(20, 50, 50, 50);
    kickAttack.setBounds(20, 100, 50, 50);
    kickRelease.setBounds(90, 100, 50, 50);
    kickAmp.setBounds(55, 150, 50, 200);
    
    snarePitch.setBounds(170, 50, 50, 50);
    snareNoise.setBounds(240, 100, 50, 50);
    snareRelease.setBounds(240, 50, 50, 50);
    snareAmp.setBounds(205, 150, 50, 200);
    
    hiPitch.setBounds(320, 50, 50, 50);
    hiRelease.setBounds(390, 50, 50, 50);
    hiAmp.setBounds(355, 150, 50, 200);
    
    lowPitch.setBounds(470, 50, 50, 50);
    lowRelease.setBounds(540, 50, 50, 50);
    lowAmp.setBounds(505, 150, 50, 200);
    
    chihatRelease.setBounds(620, 50, 50, 50);
    ohihatRelease.setBounds(690, 50, 50, 50);
    hihatTone.setBounds(620, 100, 50, 50);
    chihatAmp.setBounds(620, 150, 50, 200);
    ohihatAmp.setBounds(690, 150, 50, 200);
    
    clapPitch.setBounds(770, 50, 50, 50);
    clapAmp.setBounds(770, 150, 50, 200);
    
    cowBellPitch.setBounds(840, 50, 50, 50);
    cowBellAmp.setBounds(840, 150, 50, 200);
}

void MainComponent::paint (Graphics& g)
{
    g.setColour(Colours::black);
    g.drawSingleLineText ("Kick", 67, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("Snare", 216, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("Hi Tom", 360, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("Low Tom", 505, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("CL HH", 627, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("OP HH", 697, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("Clap", 780, 360);
    
    g.setColour(Colours::black);
    g.drawSingleLineText ("CowBell", 843, 360);
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if (slider == &kickPitch || slider == &kickAmp || slider == &kickAttack || slider == &kickRelease)
    {
        audio.setKick( (kickPitch.getValue() * 22 + 440), (kickAttack.getValue() / 100), (kickRelease.getValue() * 200), (kickAmp.getValue() / 10));
    }
    
    if (slider == &snarePitch || slider == &snareAmp || slider == &snareRelease )
    {
        audio.setHiTom( (snarePitch.getValue() * 22 + 440), (snareRelease.getValue() * 20), (snareAmp.getValue() / 10));
    }
    
    if (slider == &hiPitch || slider == &hiAmp || slider == &hiRelease )
    {
        audio.setHiTom( (hiPitch.getValue() * 22 + 660), (kickRelease.getValue() * 20), (hiAmp.getValue() / 10));
    }
    
    if (slider == &lowPitch || slider == &lowAmp || slider == &lowRelease )
    {
        audio.setLowTom( (lowPitch.getValue() * 44 + 440), (lowRelease.getValue() * 20), (lowAmp.getValue() / 10));
    }
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

