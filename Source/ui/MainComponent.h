/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../../JuceLibraryCode/JuceHeader.h"
#include "../audio/Audio.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MenuBarModel,
                        public Slider::Listener
{
public:
    //==============================================================================
    /** Constructor */
    MainComponent (Audio& audio_);

    /** Destructor */
    ~MainComponent();

    void resized() override;
    void paint (Graphics& g) override;
    void sliderValueChanged(Slider* slider) override;
    
    //MenuBarEnums/Callbacks========================================================
    enum Menus
    {
        FileMenu=0,
        
        NumMenus
    };
    
    enum FileMenuItems
    {
        AudioPrefs = 1,
        
        NumFileItems
    };
    StringArray getMenuBarNames() override;
    PopupMenu getMenuForIndex (int topLevelMenuIndex, const String& menuName) override;
    void menuItemSelected (int menuItemID, int topLevelMenuIndex) override;
    
private:
    Audio& audio;
    Slider  masterVolume,
            kickPitch,
            kickAttack,
            kickRelease,
            kickAmp,
    
            snarePitch,
            snareNoise,
            snareRelease,
            snareAmp,
    
            hiPitch,
            hiRelease,
            hiAmp,
    
            lowPitch,
            lowRelease,
            lowAmp,
    
            chihatRelease,
            chihatAmp,
            ohihatRelease,
            hihatTone,
            ohihatAmp,
    
            clapPitch,
            clapAmp,
    
            cowBellPitch,
            cowBellAmp;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
